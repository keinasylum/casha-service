package ru.galeev.casha.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.casha.entities.AbstractOperation;
import ru.galeev.casha.entities.OperationType;
import ru.galeev.casha.entities.transferObjects.DescriptionInfo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Repository
@Transactional
public class AbstractOperationService {

    @PersistenceContext
    private EntityManager manager;

    public List<AbstractOperation> getAll() {
        return manager.createNamedQuery("AbstractOperation.getAll", AbstractOperation.class).getResultList();
    }

    public List<DescriptionInfo> getUniqueDescriptions() {
        List<AbstractOperation> operations = getAll();
        List<DescriptionInfo> result = new ArrayList<>();
        Set<String> uniqueDescriptions = new HashSet<>();
        for (AbstractOperation operation : operations) {
            String description = operation.description;
            if (!uniqueDescriptions.contains(description)) {
                if (description != null && !Objects.equals(description, "")) {
                    uniqueDescriptions.add(description);
                    OperationType operationType = operation.operationType;
                    DescriptionInfo info = new DescriptionInfo();
                    info.name = description;
                    info.operationType = operationType;
                    result.add(info);
                }
            }
        }
        return result;
    }

    public Set<String> getUniqueDescription(OperationType src) {
        List<AbstractOperation> operations = getAll();
        Set<String> uniqueDescriptions = new HashSet<>();
        for (AbstractOperation operation : operations) {
            String description = operation.description;
            OperationType operationType = operation.operationType;
            if (!uniqueDescriptions.contains(description) && operationType == src) {
                if (description != null && !Objects.equals(description, "")) {
                    uniqueDescriptions.add(description);
                }
            }
        }
        return uniqueDescriptions;
    }
}
