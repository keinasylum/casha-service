package ru.galeev.casha.services;

import org.springframework.stereotype.Service;
import ru.galeev.casha.entities.BalanceHistory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class BalanceHistoryService {

    @PersistenceContext
    private EntityManager manager;

    public List<BalanceHistory> getAll() {
        return manager.createNamedQuery("BalanceHistory.getAll", BalanceHistory.class).getResultList();
    }

    public List<BalanceHistory> getByOperationId (Long operationId) {
        TypedQuery<BalanceHistory> query = manager.createNamedQuery("BalanceHistory.getByOperationId", BalanceHistory.class);
        query.setParameter("operationId", operationId);
        return query.getResultList();
    }
}