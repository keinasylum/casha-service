package ru.galeev.casha.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.casha.entities.OperationType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
@Transactional
public class OperationTypeService {

    public List<OperationType> getAll() {
        List<OperationType> types = new ArrayList<OperationType>();
        Collections.addAll(types, OperationType.values());
        return types;
    }
}
