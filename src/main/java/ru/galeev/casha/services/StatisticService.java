package ru.galeev.casha.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.galeev.casha.entities.Operation;
import ru.galeev.casha.entities.OperationType;
import ru.galeev.casha.entities.transferObjects.IncomeExpense;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class StatisticService {

    @Autowired
    private OperationService operationService;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public IncomeExpense getIncomeExpense(String start, String end) throws ParseException {
        Date startDate = dateFormat.parse(start);
        Date endDate = dateFormat.parse(end);
        List<Operation> expenseOperations = operationService.getByDateAndOperationType(startDate, endDate, OperationType.expense);
        List<Operation> incomeOperations = operationService.getByDateAndOperationType(startDate, endDate, OperationType.income);
        BigDecimal expenses = calculateCoasts(expenseOperations);
        BigDecimal incomes = calculateCoasts(incomeOperations);
        return new IncomeExpense(incomes, expenses, startDate, endDate);
    }

    private BigDecimal calculateCoasts(List<Operation> operations) {
        BigDecimal result = BigDecimal.ZERO;
        for (Operation operation : operations) {
            result = result.add(operation.cost);
        }
        return result;
    }
}
