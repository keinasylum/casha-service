package ru.galeev.casha.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import ru.galeev.casha.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class TransferService {

    private enum Direction {from, to}

    @Autowired
    private AccountService accountService;

    @PersistenceContext
    private EntityManager manager;

    public Transfer save(@Valid Transfer src) {
        BalanceHistory fromHistory = transact(src.from, src, Direction.from, false);
        fromHistory.operation = src;
        BalanceHistory toHistory = transact(src.to, src, Direction.to, false);
        toHistory.operation = src;
        manager.persist(fromHistory);
        manager.persist(toHistory);
        manager.persist(src);
        manager.flush();
        return src;
    }

    public Transfer update(@Valid Transfer src) {
        BalanceHistory fromHistory = transact(src.from, src, Direction.from, true);
        fromHistory.operation = src;
        BalanceHistory toHistory = transact(src.to, src, Direction.to, true);
        toHistory.operation = src;
        manager.persist(fromHistory);
        manager.persist(toHistory);
        manager.merge(src);
        manager.flush();
        return src;
    }

    public Transfer disable(Long id) {
        Transfer transfer = find(id);
        transfer.disabled = true;
        BalanceHistory fromHistory = disable(Direction.from, transfer.from, transfer);
        fromHistory.operation = transfer;
        BalanceHistory toHistory = disable(Direction.to, transfer.to, transfer);
        toHistory.operation = transfer;
        manager.persist(fromHistory);
        manager.persist(toHistory);
        manager.merge(transfer);
        manager.flush();
        return transfer;
    }

    public Transfer find(Long id) {
        return manager.find(Transfer.class, id);
    }

    public List<Transfer> getAll() {
        return manager.createNamedQuery("Transfer.getAll", Transfer.class).getResultList();
    }

    private BalanceHistory disable(Direction direction, Account account, AbstractOperation operation) {
        BalanceHistory history = new BalanceHistory();
        history.date = new Date();
        BigDecimal rollback = getRollbackBalance(account, operation, direction);
        history.before = account.balance;
        account.balance = rollback;
        history.after = rollback;
        history.cost = operation.cost;
        history.action = Action.transferDelete;
        manager.merge(account);
        history.account = account;
        return history;
    }

    private BalanceHistory transact(Account account, AbstractOperation operation, Direction direction, boolean updated) {
        BalanceHistory history = new BalanceHistory();
        history.date = new Date();
        history.cost = operation.cost;
        BigDecimal repositoryBalance;
        if (updated) {
            history.action = Action.transferUpdate;
            repositoryBalance = getRollbackBalance(account, operation, direction);
        } else {
            history.action = Action.transferSave;
            repositoryBalance = account.balance;
        }
        history.before = repositoryBalance;
        BigDecimal updatedBalance = null;
        if (direction == Direction.from) {
            updatedBalance = repositoryBalance.subtract(operation.cost);
            isNotNegative(updatedBalance, "TransferService: balance less zero after operation");
        }
        if (direction == Direction.to) {
            updatedBalance = repositoryBalance.add(operation.cost);
        }
        Assert.notNull(updatedBalance, "TransferService: invalid direction");
        account.balance = updatedBalance;
        history.after = updatedBalance;
        manager.merge(account);
        history.account = account;
        return history;
    }

    private BigDecimal getRollbackBalance(Account from, AbstractOperation operation, Direction direction) {
        manager.detach(from);
        manager.detach(operation);
        Account repositoryAccount = accountService.find(from.id);
        BigDecimal accountBalance = repositoryAccount.balance;
        BigDecimal rollbackAmount = find(operation.id).cost;
        BigDecimal updatedBalance = null;
        if (direction == Direction.from) {
            updatedBalance = accountBalance.add(rollbackAmount);
        }
        if (direction == Direction.to) {
            updatedBalance = accountBalance.subtract(rollbackAmount);
        }
        Assert.notNull(updatedBalance, "TransferService: invalid operationType");
        return updatedBalance;
    }

    private void isNotNegative(BigDecimal src, String msg) {
        Assert.isTrue(src.compareTo(BigDecimal.ZERO) == 1, msg);
    }
}
