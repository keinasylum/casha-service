package ru.galeev.casha.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import ru.galeev.casha.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.*;

@Repository
@Transactional
public class OperationService {

    @PersistenceContext
    private EntityManager manager;

    public Operation save(@Valid Operation src) {
        BalanceHistory history = transact(src, false);
        manager.persist(src);
        manager.persist(history);
        manager.flush();
        return src;
    }

    public Operation update(@Valid Operation src) {
        BalanceHistory history = transact(src, true);
        manager.merge(src);
        manager.persist(history);
        manager.flush();
        return src;
    }

    public Operation disable(Long id) {
        BalanceHistory history = new BalanceHistory();
        history.date = new Date();
        Operation operation = find(id);
        operation.disabled = true;
        history.operation = operation;
        Account account = operation.account;
        BigDecimal rollback = getRollbackBalance(operation);
        history.before = account.balance;
        account.balance = rollback;
        history.after = rollback;
        history.cost = operation.cost;
        history.action = Action.operationDelete;
        manager.merge(operation.account);
        history.account = operation.account;
        manager.persist(history);
        manager.merge(operation);
        manager.flush();
        return operation;
    }

    public Operation find(Long id) {
        return manager.find(Operation.class, id);
    }

    public List<Operation> getAll() {
        return manager.createNamedQuery("Operation.getAll", Operation.class).getResultList();
    }

    public List<Operation> getByDateAndOperationType(Date start, Date end, OperationType type) {
        TypedQuery<Operation> query = manager.createNamedQuery("Operation.getByDateAndOperationType", Operation.class);
        query.setParameter("startDate", start);
        query.setParameter("endDate", end);
        query.setParameter("operationType", type);
        return query.getResultList();
    }

    private BalanceHistory transact(Operation src, boolean updated) {
        BalanceHistory history = new BalanceHistory();
        history.date = new Date();
        history.operation = src;
        history.cost = src.cost;
        Account operationAccount = src.account;
        BigDecimal repositoryBalance;
        if (updated) {
            history.action = Action.operationUpdate;
            repositoryBalance = getRollbackBalance(src);
        } else {
            history.action = Action.operationSave;
            repositoryBalance = operationAccount.balance;
        }
        history.before = repositoryBalance;
        OperationType type = src.operationType;
        BigDecimal updatedBalance = null;
        if (type == OperationType.expense) {
            updatedBalance = repositoryBalance.subtract(src.cost);
            isNotNegative(updatedBalance, "OperationService: balance less zero after operation");
        }
        if (type == OperationType.income) {
            updatedBalance = repositoryBalance.add(src.cost);
        }
        Assert.notNull(updatedBalance, "OperationService: invalid operationType");
        operationAccount.balance = updatedBalance;
        history.after = updatedBalance;
        manager.merge(operationAccount);
        history.account = operationAccount;
        return history;
    }

    private BigDecimal getRollbackBalance(Operation src) {
        manager.detach(src);
        Operation repositoryOperation = find(src.id);
        BigDecimal accountBalance = repositoryOperation.account.balance;
        BigDecimal updatedBalance = null;
        OperationType type = repositoryOperation.operationType;
        BigDecimal rollbackAmount = repositoryOperation.cost;
        if (type == OperationType.expense) {
            updatedBalance = accountBalance.add(rollbackAmount);
        }
        if (type == OperationType.income) {
            updatedBalance = accountBalance.subtract(rollbackAmount);
        }
        Assert.notNull(updatedBalance, "OperationService: invalid operationType");
        return updatedBalance;
    }

    private void isNotNegative(BigDecimal src, String msg) {
        Assert.isTrue(src.compareTo(BigDecimal.ZERO) == 1, msg);
    }
}
