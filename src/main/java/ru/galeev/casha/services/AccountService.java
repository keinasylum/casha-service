package ru.galeev.casha.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.casha.entities.Account;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public class AccountService {

    @PersistenceContext
    private EntityManager manager;

    public Account save(@Valid Account src) {
        manager.persist(src);
        manager.flush();
        return src;
    }

    public Account update(@Valid Account src) {
        manager.merge(src);
        manager.flush();
        return src;
    }

    public Account remove(Long id) {
        Account account = find(id);
        manager.remove(account);
        return account;
    }

    public Account find(Long id) {
        return manager.find(Account.class, id);
    }

    public List<Account> getAll() {
        TypedQuery<Account> query = manager.createNamedQuery("Account.getAll", Account.class);
        return query.getResultList();
    }

    public BigDecimal getTotalBalance() {
        List<Account> accounts = getAll();
        BigDecimal result = new BigDecimal(0L);
        for (Account account : accounts) {
            result = result.add(account.balance);
        }
        return result;
    }
}
