package ru.galeev.casha.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.casha.entities.Category;
import ru.galeev.casha.entities.OperationType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.util.List;

@Repository
@Transactional
public class CategoryService {

    @PersistenceContext
    private EntityManager manager;

    public Category save(@Valid Category src) {
        manager.persist(src);
        manager.flush();
        return src;
    }

    public List<Category> getAll() {
        TypedQuery<Category> query = manager.createNamedQuery("Category.getAll", Category.class);
        return query.getResultList();
    }

    public Category update(@Valid Category src) {
        manager.merge(src);
        manager.flush();
        return src;
    }

    public Category delete(Long id) {
        Category category = find(id);
        manager.remove(category);
        return category;
    }

    public List<Category> getByOperationType(OperationType operationType) {
        TypedQuery<Category> query = manager.createNamedQuery("Category.getByOperationType", Category.class);
        query.setParameter("operationType", operationType);
        return query.getResultList();
    }

    public Category find(Long id) {
        return manager.find(Category.class, id);
    }
}
