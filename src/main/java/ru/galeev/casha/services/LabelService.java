package ru.galeev.casha.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.casha.entities.Label;
import ru.galeev.casha.entities.OperationType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.util.List;

@Repository
@Transactional
public class LabelService {

    @PersistenceContext
    private EntityManager manager;

    public List<Label> getAll() {
        TypedQuery<Label> query = manager.createNamedQuery("Label.getAll", Label.class);
        List<Label> result = query.getResultList();
        return result;
    }

    public List<Label> getByOperationType(OperationType operationType) {
        TypedQuery<Label> query = manager.createNamedQuery("Label.getByOperationType", Label.class);
        query.setParameter("operationType", operationType);
        List<Label> result = query.getResultList();
        return result;
    }

    public Label find(Long id) {
        return manager.find(Label.class, id);
    }

    public Label delete(Long id) {
        Label label = find(id);
        manager.remove(label);
        return label;
    }

    public Label save(@Valid Label src) {
        manager.persist(src);
        manager.flush();
        return src;
    }

    public Label update(@Valid Label src) {
        manager.merge(src);
        manager.flush();
        return src;
    }
}