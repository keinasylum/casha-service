package ru.galeev.casha.entities;

public enum  Action {
    operationSave(1, "operationSave"),
    operationUpdate(2, "operationUpdate"),
    operationDelete(3, "operationDelete"),
    transferSave(4, "transferSave"),
    transferUpdate(5, "transferUpdate"),
    transferDelete(6, "transferDelete");

    public final long id;

    public final String name;

    Action(long id, String name) {
        this.id = id;
        this.name = name;
    }
}