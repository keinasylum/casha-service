package ru.galeev.casha.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@NamedQueries({
        @NamedQuery(name = "Label.getAll", query = "select s from Label s"),
        @NamedQuery(name = "Label.getByOperationType", query = "select s from Label s where s.operationType = :operationType"),
        @NamedQuery(name = "Label.getByName", query = "select s from Label s where s.name = :name")
})
public class Label {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @NotNull(message = "Label.name must not be null")
    @Size(max = 50, message = "Label.name length must be less 50")
    @NotEmpty(message = "Label.name must not be empty")
    @Column(nullable = false, unique = true, length = 50)
    public String name;

    @NotNull(message = "Label.operationType must not be null")
    @Column(nullable = false)
    public OperationType operationType;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn
    public Set<Category> categories;
}
