package ru.galeev.casha.entities.transferObjects;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import ru.galeev.casha.serialization.JsonDateSerializer;

import java.math.BigDecimal;
import java.util.Date;

public class IncomeExpense {

    public BigDecimal income;

    public BigDecimal expense;

    @JsonSerialize(using = JsonDateSerializer.class)
    public Date start;

    @JsonSerialize(using = JsonDateSerializer.class)
    public Date end;

    public IncomeExpense(BigDecimal income, BigDecimal expense, Date start, Date end) {
        this.income = income;
        this.expense = expense;
        this.start = start;
        this.end = end;
    }
}
