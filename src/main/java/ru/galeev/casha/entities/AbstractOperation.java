package ru.galeev.casha.entities;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import ru.galeev.casha.serialization.JsonDateSerializer;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "AbstractOperation.getAll", query = "select c from AbstractOperation c where c.disabled = false order by c.date asc")
})
public abstract class AbstractOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Min(value = 0, message = "AbstractOperation.cost must be more than 0")
    @NotNull(message = "AbstractOperation.cost must not be null")
    @Column(nullable = false)
    public BigDecimal cost;

    @NotNull(message = "AbstractOperation.date must not be null")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date date;

    @NotNull(message = "AbstractOperation.operationType must not be null")
    @Enumerated
    public OperationType operationType;

    @Size(max = 150, message = "Operation.description length must be less 150")
    @Column
    public String description;

    @Column(nullable = false)
    @JsonIgnore
    public boolean disabled = false;
}