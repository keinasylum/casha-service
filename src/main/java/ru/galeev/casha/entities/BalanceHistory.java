package ru.galeev.casha.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import ru.galeev.casha.serialization.JsonDateSerializer;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "BalanceHistory.getAll", query = "select c from BalanceHistory c"),
        @NamedQuery(name = "BalanceHistory.getByOperationId", query = "select c from BalanceHistory c where c.operation.id = :operationId")
})
public class BalanceHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

//    @NotNull(message = "BalanceHistory.operation must not be null")
    @ManyToOne
    public AbstractOperation operation;

    @NotNull(message = "BalanceHistory.account must not be null")
    @ManyToOne
    public Account account;

    @NotNull(message = "BalanceHistory.before must not be null")
    @Min(value = 0, message = "Min BalanceHistory.before must be more than 0")
    @Column(nullable = false)
    public BigDecimal before;

    @NotNull(message = "BalanceHistory.after must not be null")
    @Min(value = 0, message = "Min BalanceHistory.after must be more than 0")
    @Column(nullable = false)
    public BigDecimal after;

    @NotNull(message = "BalanceHistory.cost must not be null")
    @Min(value = 0, message = "Min BalanceHistory.cost must be more than 0")
    @Column(nullable = false)
    public BigDecimal cost;

    @NotNull(message = "BalanceHistory.date must not be null")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date date;

    @NotNull(message = "BalanceHistory.action must not be null")
    @Enumerated
    public Action action;
}