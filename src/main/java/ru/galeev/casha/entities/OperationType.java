package ru.galeev.casha.entities;

import org.springframework.util.Assert;

public enum OperationType {
    income(0, "Доход"),
    expense(1, "Расход"),
    transfer(4, "Перевод");

    public final long id;

    public final String name;

    OperationType(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public static OperationType getByName(String typeName) {
        Assert.notNull(typeName);
        String lowCaseName = typeName.toLowerCase();
        OperationType result = null;
        if (lowCaseName.equals("income")) {
            result = OperationType.income;
        }
        if (lowCaseName.equals("expense")) {
            result = OperationType.expense;
        }
        Assert.notNull(result);
        return result;
    }
}
