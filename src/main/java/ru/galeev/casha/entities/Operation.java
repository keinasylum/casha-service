package ru.galeev.casha.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.Set;

@Entity
@NamedQueries({
        @NamedQuery(name = "Operation.getAll", query = "select c from Operation c where c.disabled = false order by c.date asc"),
        @NamedQuery(name = "Operation.getAllDESC", query = "select c from Operation c where c.disabled = false order by c.date desc"),
        @NamedQuery(name = "Operation.getByDateAndOperationType",
                query = "select c from Operation c where c.date >= :startDate and c.date <= :endDate and c.operationType = :operationType and c.disabled = false"),
        @NamedQuery(name = "Operation.getByDescription", query = "select c from Operation c where c.description = :description and c.disabled = false"),
        @NamedQuery(name = "Operation.getCount", query = "select count(c.id) from Operation c where c.disabled = false")
})
public class Operation extends AbstractOperation {

    @NotNull(message = "Operation.account must not be null")
    @ManyToOne
    public Account account;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable
    public Set<Label> labels;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation that = (Operation) o;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(id, that.id);
        builder.append(cost, that.cost);
        builder.append(date, that.date);
        builder.append(account, that.account);
        builder.append(description, that.description);
        builder.append(operationType, that.operationType);
        builder.append(labels, that.labels);
        builder.append(disabled, that.disabled);
        return builder.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(id);
        builder.append(cost);
        builder.append(date);
        builder.append(description);
        builder.append(disabled);
        return builder.hashCode();
    }
}
