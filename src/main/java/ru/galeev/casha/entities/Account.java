package ru.galeev.casha.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@NamedQueries({
        @NamedQuery(name = "Account.getAll", query = "select c from Account c"),
        @NamedQuery(name= "Account.getByName", query = "select c from Account c where c.name = :name")
})
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @NotNull(message = "Account.name must not be null")
    @Size(min = 1, max = 50, message = "Account.name length must be between 1 and 50")
    @NotEmpty(message = "Account.name must not be empty")
    @Column(nullable = false, unique = true, length = 50)
    public String name;

    @NotNull(message = "Account.balance must not be null")
    @Min(value = 0, message = "The minimum Account.balance must be more 0")
    @Column(nullable = false)
    public BigDecimal balance;

    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        } else if(o.getClass() != this.getClass()) {
            return false;
        }
        Account that = (Account) o;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(balance.intValue(), that.balance.intValue());
        builder.append(id, that.id);
        builder.append(name, that.name);
        return builder.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(balance);
        builder.append(id);
        builder.append(name);
        return builder.hashCode();
    }
}