package ru.galeev.casha.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
        @NamedQuery(name = "Transfer.getByOperation", query = "select c from Transfer c where c.from = :operation or c.to = :operation"),
        @NamedQuery(name = "Transfer.getAll", query = "select c from Transfer c where c.disabled = false order by c.date asc")
})
public class Transfer extends AbstractOperation {

    @NotNull(message = "Transfer.from must not be null")
    @ManyToOne
    public Account from;

    @NotNull(message = "Transfer.to must not be null")
    @ManyToOne
    public Account to;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer that = (Transfer) o;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(id, that.id);
        builder.append(cost, that.cost);
        builder.append(date, that.date);
        builder.append(from, that.from);
        builder.append(to, that.to);
        builder.append(description, that.description);
        builder.append(operationType, that.operationType);
        builder.append(disabled, that.disabled);
        return builder.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(id);
        builder.append(cost);
        builder.append(date);
        builder.append(description);
        builder.append(disabled);
        return builder.hashCode();
    }
}
