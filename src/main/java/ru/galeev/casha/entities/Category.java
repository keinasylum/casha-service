package ru.galeev.casha.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
        @NamedQuery(name = "Category.getAll", query = "select s from Category s"),
        @NamedQuery(name = "Category.getByOperationType", query = "select s from Category s where s.operationType = :operationType")
})
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @NotNull(message = "Category.name must not be null")
    @Size(max = 50, message = "Category.name length must be less 50")
    @NotEmpty(message = "Category.name must not be empty")
    @Column(nullable = false, unique = true, length = 50)
    public String name;

    @NotNull(message = "Category.operationType must not be null")
    @Column(nullable = false)
    public OperationType operationType;
}
