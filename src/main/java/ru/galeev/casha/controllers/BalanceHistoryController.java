package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.BalanceHistory;
import ru.galeev.casha.services.BalanceHistoryService;

import java.util.List;

@RestController
@RequestMapping("/balanceHistory")
public class BalanceHistoryController {

    @Autowired
    private BalanceHistoryService service;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<BalanceHistory> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "getByOperationId/{id}", method = RequestMethod.GET)
    @ResponseBody
    List<BalanceHistory> getByOperationId(@PathVariable Long id) {
        return service.getByOperationId(id);
    }
}