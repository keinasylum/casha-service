package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.Operation;
import ru.galeev.casha.services.OperationService;

import java.util.List;


@RestController
@RequestMapping("/operation")
public class OperationController {

    @Autowired
    private OperationService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    Operation save(@RequestBody Operation operation) {
        return service.save(operation);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Operation update(@RequestBody Operation request) {
        return service.update(request);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    Operation disable(@PathVariable Long id) {
        return service.disable(id);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<Operation> getAll() {
        return service.getAll();
    }
}
