package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.Category;
import ru.galeev.casha.entities.OperationType;
import ru.galeev.casha.services.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    Category save(@RequestBody Category category) {
        return service.save(category);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Category update(@RequestBody Category category) {
        return service.update(category);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<Category> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    Category delete(@PathVariable Long id) {
        return service.delete(id);
    }

    @RequestMapping(value = "byOperationType", method = RequestMethod.GET)
    @ResponseBody
    List<Category> getByOperationType(@RequestParam String typeName) {
        return service.getByOperationType(OperationType.getByName(typeName));
    }
}
