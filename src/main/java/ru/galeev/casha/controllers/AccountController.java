package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.Account;
import ru.galeev.casha.services.AccountService;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    Account save(@RequestBody Account request) {
        return service.save(request);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Account update(@RequestBody Account request) {
        return service.update(request);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<Account> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    Account remove(@PathVariable Long id) {
        return service.remove(id);
    }

    @RequestMapping(value = "/totalBalance", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    BigDecimal getTotalBalance() { return service.getTotalBalance(); }
}