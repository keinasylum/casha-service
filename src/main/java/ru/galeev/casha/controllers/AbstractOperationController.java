package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.AbstractOperation;
import ru.galeev.casha.entities.OperationType;
import ru.galeev.casha.entities.transferObjects.DescriptionInfo;
import ru.galeev.casha.services.AbstractOperationService;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/abstractOperation")
public class AbstractOperationController {

    @Autowired
    private AbstractOperationService service;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<AbstractOperation> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "allUniqueDescription", method = RequestMethod.GET)
    List<DescriptionInfo> getUniqueDescription() {
        return service.getUniqueDescriptions();
    }

    @RequestMapping(value = "uniqueDescription", method = RequestMethod.GET)
    @ResponseBody
    Set<String> getUniqueDescription(@RequestParam OperationType operationType) {
        return service.getUniqueDescription(operationType);
    }
}