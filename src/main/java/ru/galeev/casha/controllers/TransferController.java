package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.Transfer;
import ru.galeev.casha.services.TransferService;

import java.util.List;

@RestController
@RequestMapping("/transfer")
public class TransferController {

    @Autowired
    private TransferService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    Transfer save(@RequestBody Transfer transfer) {
        return service.save(transfer);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Transfer update(@RequestBody Transfer transfer) {
        return service.update(transfer);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    Transfer disable(@PathVariable Long id) {
        return service.disable(id);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<Transfer> getAll() {
        return service.getAll();
    }
}
