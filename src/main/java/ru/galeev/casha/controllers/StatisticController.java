package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.services.StatisticService;
import ru.galeev.casha.entities.transferObjects.IncomeExpense;

import java.text.ParseException;

@RestController
@RequestMapping("/statistic")
public class StatisticController {

    @Autowired
    private StatisticService service;

    @RequestMapping(value = "incomeExpense", method = RequestMethod.GET)
    @ResponseBody
    IncomeExpense getIncomeExpense(@RequestParam String start, @RequestParam String end) throws ParseException {
        return service.getIncomeExpense(start, end);
    }
}