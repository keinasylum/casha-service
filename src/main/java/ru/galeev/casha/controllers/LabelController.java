package ru.galeev.casha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.casha.entities.Label;
import ru.galeev.casha.entities.OperationType;
import ru.galeev.casha.services.LabelService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/label")
public class LabelController {

    @Autowired
    private LabelService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    Label save(@RequestBody @Valid Label request) {
        return service.save(request);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Label update(@RequestBody @Valid Label request) {
        return service.update(request);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @ResponseBody
    List<Label> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "byOperationType", method = RequestMethod.GET)
    @ResponseBody
    List<Label> getByOperationType(@RequestParam String typeName) {
        return service.getByOperationType(OperationType.getByName(typeName));
}

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    Label delete(@PathVariable Long id) {
        return service.delete(id);
    }
}
