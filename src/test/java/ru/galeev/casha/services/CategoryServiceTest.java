package ru.galeev.casha.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.galeev.casha.entities.Category;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(locations = {"classpath:test-spring-config.xml"})
public class CategoryServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private CategoryService service;

    @Autowired
    private TestDataCreator creator;

    @Test
    public void testSave() throws Exception {
        Category category = creator.createCategory();

        Category result = service.save(category);

        Category expected = creator.getCategorySingleResult();
        assertThat(result, is(expected));
    }
}
