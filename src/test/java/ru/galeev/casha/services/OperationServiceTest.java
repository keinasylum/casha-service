package ru.galeev.casha.services;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.galeev.casha.entities.*;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(locations = {"classpath:test-spring-config.xml"})
public class OperationServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private OperationService service;

    @Autowired
    private TestDataCreator creator;

    @Test(expected = ConstraintViolationException.class)
    public void testNegativeCost() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.cost = new BigDecimal(-100L);
        service.save(operation);
    }

    @Ignore
    @Test(expected = ConstraintViolationException.class)
    public void testNullCost() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.cost = null;
        service.save(operation);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testNullDate() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.date = null;
        service.save(operation);
    }

    @Ignore
    @Test(expected = ConstraintViolationException.class)
    public void testNullAccount() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.account = null;
        service.save(operation);
    }

    @Test
    public void testNullDescription() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.description = null;
        service.save(operation);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testLongDescription() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.description = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
        service.save(operation);
    }

    @Ignore
    @Test(expected = ConstraintViolationException.class)
    public void testNullOperationType() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));
        operation.operationType = null;
        service.save(operation);
    }

    @Test
    public void testSave_expense() throws Exception {
        Operation operation = creator.createOperation(OperationType.expense, new BigDecimal(100L));

        Operation result = service.save(operation);

        Account repositoryAccount = creator.getAccountSingleResult();
        assertThat(operation.account, is(repositoryAccount));
        assertThat(repositoryAccount.balance, is(new BigDecimal(900L)));
        BalanceHistory history = creator.getByOperationId(result.id);
        assertThat(history.account, is(result.account));
        assertThat(history.before, is(new BigDecimal(1000L)));
        assertThat(history.after, is(new BigDecimal(900L)));
        assertThat(history.cost, is(operation.cost));
        assertThat(history.action, is(Action.operationSave));
    }

    @Test
    public void testSave_income() throws Exception {
        Operation operation = creator.createOperation(OperationType.income, new BigDecimal(100L));

        Operation result = service.save(operation);

        Account repositoryAccount = creator.getAccountSingleResult();
        assertThat(operation.account, is(repositoryAccount));
        assertThat(repositoryAccount.balance, is(new BigDecimal(1100L)));
        BalanceHistory history = creator.getByOperationId(result.id);
        assertThat(history.account, is(result.account));
        assertThat(history.before, is(new BigDecimal(1000L)));
        assertThat(history.after, is(new BigDecimal(1100L)));
        assertThat(history.cost, is(operation.cost));
        assertThat(history.action, is(Action.operationSave));
    }

    @Test
    public void testUpdate_expense() throws Exception {
        Operation operation = creator.persistOperation(OperationType.expense, new BigDecimal(100L));
        operation.cost = new BigDecimal(200L);

        Operation result = service.update(operation);

        Account repositoryAccount = creator.getAccountSingleResult();
        assertThat(operation.account, is(repositoryAccount));
        assertThat(repositoryAccount.balance.intValue(), is(900));
        BalanceHistory history = creator.getByOperationId(result.id);
        assertThat(history.account, is(result.account));
        assertThat(history.before.intValue(), is(1100));
        assertThat(history.after.intValue(), is(900));
        assertThat(history.cost, is(operation.cost));
        assertThat(history.action, is(Action.operationUpdate));
    }

    @Test
    public void testUpdate_income() throws Exception {
        Operation operation = creator.persistOperation(OperationType.income, new BigDecimal(100L));
        operation.cost = new BigDecimal(200L);

        Operation result = service.update(operation);

        Account repositoryAccount = creator.getAccountSingleResult();
        assertThat(operation.account, is(repositoryAccount));
        assertThat(repositoryAccount.balance.intValue(), is(1100));
        BalanceHistory history = creator.getByOperationId(result.id);
        assertThat(history.account, is(result.account));
        assertThat(history.before.intValue(), is(900));
        assertThat(history.after.intValue(), is(1100));
        assertThat(history.cost, is(operation.cost));
        assertThat(history.action, is(Action.operationUpdate));
    }

    @Test
    public void testDelete_income() throws Exception {
        Operation operation = creator.persistOperation(OperationType.income, new BigDecimal(100L));

        Operation result = service.disable(operation.id);

        assertThat(operation.disabled, is(true));
        Account repositoryAccount = creator.getAccountSingleResult();
        assertThat(operation.account, is(repositoryAccount));
        assertThat(repositoryAccount.balance.intValue(), is(900));
        BalanceHistory history = creator.getByOperationId(result.id);
        assertThat(history.account, is(result.account));
        assertThat(history.before.intValue(), is(1000));
        assertThat(history.after.intValue(), is(900));
        assertThat(history.cost, is(operation.cost));
        assertThat(history.action, is(Action.operationDelete));
    }

    @Test
    public void testDelete_expense() throws Exception {
        Operation operation = creator.persistOperation(OperationType.expense, new BigDecimal(100L));

        Operation result = service.disable(operation.id);

        assertThat(operation.disabled, is(true));
        Account repositoryAccount = creator.getAccountSingleResult();
        assertThat(operation.account, is(repositoryAccount));
        assertThat(repositoryAccount.balance.intValue(), is(1100));
        BalanceHistory history = creator.getByOperationId(result.id);
        assertThat(history.account, is(result.account));
        assertThat(history.before.intValue(), is(1000));
        assertThat(history.after.intValue(), is(1100));
        assertThat(history.cost, is(operation.cost));
        assertThat(history.action, is(Action.operationDelete));
    }

    @Test
    public void testGetAll() throws Exception {
        List<Operation> operations = creator.persistOperationList();

        List<Operation> result = service.getAll();

        assertThat(result, is(operations));
    }
}
