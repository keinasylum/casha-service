package ru.galeev.casha.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.galeev.casha.entities.Account;
import ru.galeev.casha.entities.Action;
import ru.galeev.casha.entities.BalanceHistory;
import ru.galeev.casha.entities.Transfer;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(locations = {"classpath:test-spring-config.xml"})
public class TransferServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private TransferService service;

    @Autowired
    private TestDataCreator creator;

    @Test
    public void testSave() throws Exception {
        Transfer transfer = creator.createTransfer(new BigDecimal(100));

        service.save(transfer);

        Account from = creator.find(transfer.from.id);
        assertThat(from.balance.intValue(), is(900));
        Account to = creator.find(transfer.to.id);
        assertThat(to.balance.intValue(), is(1100));
        List<BalanceHistory> histories = creator.getAll();
        BalanceHistory historyFrom = histories.get(0);
        assertThat(historyFrom.account, is(from));
        assertThat(historyFrom.before.intValue(), is(1000));
        assertThat(historyFrom.after.intValue(), is(900));
        assertThat(historyFrom.cost.intValue(), is(100));
        assertThat(historyFrom.action, is(Action.transferSave));
        BalanceHistory historyTo = histories.get(1);
        assertThat(historyTo.account, is(to));
        assertThat(historyTo.before.intValue(), is(1000));
        assertThat(historyTo.after.intValue(), is(1100));
        assertThat(historyTo.cost.intValue(), is(100));
        assertThat(historyTo.action, is(Action.transferSave));
    }

    @Test
    public void testUpdate() throws Exception {
        Transfer transfer = creator.persistTransfer(new BigDecimal(100));
        transfer.cost = new BigDecimal(300);

        service.update(transfer);

        Account from = creator.find(transfer.from.id);
        assertThat(from.balance.intValue(), is(800));
        Account to = creator.find(transfer.to.id);
        assertThat(to.balance.intValue(), is(1200));
        List<BalanceHistory> histories = creator.getAll();
        BalanceHistory historyFrom = histories.get(0);
        assertThat(historyFrom.account, is(from));
        assertThat(historyFrom.before.intValue(), is(1100));
        assertThat(historyFrom.after.intValue(), is(800));
        assertThat(historyFrom.cost.intValue(), is(300));
        assertThat(historyFrom.action, is(Action.transferUpdate));
        BalanceHistory historyTo = histories.get(1);
        assertThat(historyTo.account, is(to));
        assertThat(historyTo.before.intValue(), is(900));
        assertThat(historyTo.after.intValue(), is(1200));
        assertThat(historyTo.cost.intValue(), is(300));
        assertThat(historyTo.action, is(Action.transferUpdate));
    }

    @Test
    public void testDelete() throws Exception {
        Transfer transfer = creator.persistTransfer(new BigDecimal(100));

        service.disable(transfer.id);

        Account from = creator.find(transfer.from.id);
        assertThat(from.balance.intValue(), is(1100));
        Account to = creator.find(transfer.to.id);
        assertThat(to.balance.intValue(), is(900));
        List<BalanceHistory> histories = creator.getAll();
        BalanceHistory historyFrom = histories.get(0);
        assertThat(historyFrom.account, is(from));
        assertThat(historyFrom.before.intValue(), is(1000));
        assertThat(historyFrom.after.intValue(), is(1100));
        assertThat(historyFrom.cost.intValue(), is(100));
        assertThat(historyFrom.action, is(Action.transferDelete));
        BalanceHistory historyTo = histories.get(1);
        assertThat(historyTo.account, is(to));
        assertThat(historyTo.before.intValue(), is(1000));
        assertThat(historyTo.after.intValue(), is(900));
        assertThat(historyTo.cost.intValue(), is(100));
        assertThat(historyTo.action, is(Action.transferDelete));
    }
}
