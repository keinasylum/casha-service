package ru.galeev.casha.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.galeev.casha.entities.Label;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(locations = {"classpath:test-spring-config.xml"})
public class LabelServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private LabelService service;

    @Autowired
    private TestDataCreator creator;

    @Test
    public void testSave() throws Exception {
        Label label = creator.createLabel();

        Label result = service.save(label);

        Label expected = creator.getLabelSingleResult();
        assertThat(result, is(expected));
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSave_nullName() throws Exception {
        Label label = creator.createLabel();
        label.name = null;

        service.save(label);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSave_nullOperationType() throws Exception {
        Label label = creator.createLabel();
        label.operationType = null;

        service.save(label);
    }
}
