package ru.galeev.casha.services;

import org.springframework.stereotype.Component;
import ru.galeev.casha.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.*;

@Component
public class TestDataCreator {

    private static final String QUERY_SELECT_ALL_CATEGORIES = "Select c from Category c";
    private static final String QUERY_SELECT_ALL_LABELS = "Select c from Label c";

    @PersistenceContext
    private EntityManager manager;

    public Category createCategory() {
        Category category = new Category();
        category.name = "Test category";
        category.operationType = OperationType.expense;
        return category;
    }

    public Label createLabel() {
        Label label = new Label();
        label.operationType = OperationType.expense;
        label.name = "Test label";
        Set<Category> categories = new HashSet<>();
        categories.add(persistCategory());
        label.categories = categories;
        return label;
    }

    public Account createAccount(BigDecimal balance) {
        Account account = new Account();
        account.balance = balance;
        account.name = generateString(new Random(), "Test name", 10);
        return account;
    }

    public Operation createOperation(OperationType operationType, BigDecimal cost) {
        Operation operation = new Operation();
        operation.account = persistAccount(new BigDecimal(1000L));
        operation.cost = cost;
        operation.date = new Date();
        operation.description = "Test description";
        operation.operationType = operationType;
        Set<Label> labelSet = new HashSet<>();
        labelSet.add(persistLabel());
        operation.labels = labelSet;
        return operation;
    }

    public Transfer createTransfer(BigDecimal cost) {
        Transfer transfer = new Transfer();
        transfer.from = persistAccount(new BigDecimal(1000));
        transfer.to = persistAccount(new BigDecimal(1000));
        transfer.cost = cost;
        transfer.date = new Date();
        transfer.operationType = OperationType.transfer;
        return transfer;
    }

    public Category persistCategory() {
        Category category = createCategory();
        manager.persist(category);
        manager.flush();
        return category;
    }

    public Label persistLabel() {
        Label label = createLabel();
        manager.persist(label);
        manager.flush();
        return label;
    }

    public Account persistAccount(BigDecimal balance) {
        Account account = createAccount(balance);
        manager.persist(account);
        manager.flush();
        return account;
    }

    public Operation persistOperation(OperationType type, BigDecimal cost) {
        Operation operation = createOperation(type, cost);
        manager.persist(operation);
        manager.flush();
        return operation;
    }

    public Transfer persistTransfer(BigDecimal cost) {
        Transfer transfer = createTransfer(cost);
        manager.persist(transfer);
        manager.flush();
        return transfer;
    }

    public Category getCategorySingleResult() {
        return manager.createQuery(QUERY_SELECT_ALL_CATEGORIES, Category.class).getSingleResult();
    }

    public Label getLabelSingleResult() {
        return manager.createQuery(QUERY_SELECT_ALL_LABELS, Label.class).getSingleResult();
    }

    public Account getAccountSingleResult() {
        return manager.createNamedQuery("Account.getAll", Account.class).getSingleResult();
    }

    public BalanceHistory getByOperationId(Long operationId) {
        TypedQuery<BalanceHistory> query = manager.createNamedQuery("BalanceHistory.getByOperationId", BalanceHistory.class);
        query.setParameter("operationId", operationId);
        return query.getSingleResult();
    }

    public List<BalanceHistory> getAll() {
        return manager.createNamedQuery("BalanceHistory.getAll", BalanceHistory.class).getResultList();
    }

    public Account find(Long id) {
        return manager.find(Account.class, id);
    }

    public List<Operation> persistOperationList() {
        Account account = persistAccount(new BigDecimal(1000L));
        List<Operation> operations = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Operation operation = new Operation();
            operation.operationType = OperationType.expense;
            operation.cost = new BigDecimal(100L);
            operation.date = new Date();
            operation.account = account;
            manager.persist(operation);
            operations.add(operation);
        }
        manager.flush();
        return operations;
    }

    private static String generateString(Random random, String src, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = src.charAt(random.nextInt(src.length()));
        }
        return new String(text);
    }
}
